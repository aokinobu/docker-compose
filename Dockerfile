FROM docker:18-git
MAINTAINER Nobuyuki Aoki "aokinobu@gmail.com"

RUN apk update; apk upgrade; apk add libffi-dev openssl-dev python3-dev python python-dev py-pip build-base; pip install  docker-compose==1.24.1
